import math
def BubbleSort(array,start,end):
    i=0
    swapped = True
    while i<end and swapped == True:
        swapped = False
        for j in range(start,end-i):
            if(array[j] > array[j+1]):
                temp = array[j]
                array[j] = array[j+1]
                array[j+1] = temp
                swapped = True
        i+=1

def SelectionSort(array,start,end):
    for i in range(start,end+1):
        min = i
        for j in range(i+1,end+1):
            if(array[min] > array[j]):
                temp = array[min]
                array[min] = array[j]
                array[j] = temp

def HybridMergeSort(A,p,r):
    if r-p>23:
        q = math.floor((p+r)/2)
        HybridMergeSort(A,p,q)
        HybridMergeSort(A,q+1,r)
        if type(A[0])==int:
            Merge(A,p,q,r)
        elif  type(A[0])==str:
            MergeString(A, p, q, r) 
    else:
        SelectionSort(A, p, r)
        
def Merge(A,p,q,r):
    L = []
    R = []
    for i in range(0,q-p+1):
        L.append(A[p+i])
    for j in range(0,r-q):
        R.append(A[q+j+1])
    #L = A[p:q+1]
    #R = A[q+1:r+1]
    L.append(100000000000)
    R.append(100000000000)
    i,j = 0,0
    for k in range(p,r+1):
        if(L[i] < R[j]):
            A[k] = L[i]
            i=i+1
        else:
            A[k] = R[j]
            j = j+1

def InsertionSort(array,start,end):
    for i in range(start, end):
        key=array[i]
        j=i-1
        while key< array[j] and j>=0:
            array[j+1]=array[j]
            j=j-1
        array[j+1]=key
    return array

def MergeSort(array,start,end):
    if start!=end:
        mid=((start+end)/2)
        mid=math.floor(mid)
        MergeSort(array,start,mid)
        MergeSort(array,mid+1,end)
        if type(array[0])==int:
            Merge(array,start,mid,end)
        elif  type(array[0])==str:
            MergeString(array, start, mid, end)    
     
    
def HybridMergeInsertionSort(A,p,r):
    if r-p>43:
        q = math.floor((p+r)/2)
        HybridMergeInsertionSort(A, p, q)
        HybridMergeInsertionSort(A,q+1,r)
        if type(A[0])==int:
            Merge(A,p,q,r)
        elif  type(A[0])==str:
            MergeString(A, p, q, r)
    else:
        InsertionSort(A, p,r)
def MergeString(A,p,q,r):
    L = []
    R = []
    for i in range(0,q-p+1):
        L.append(A[p+i])
    for j in range(0,r-q):
        R.append(A[q+j+1])
    #L = A[p:q+1]
    #R = A[q+1:r+1]
    L.append("zzzzzzzzzzzzzzzzzzzzz")
    R.append("zzzzzzzzzzzzzzzzzzzzz")
    
    
    i,j = 0,0
    for k in range(p,r+1):
        LA = makeAscii(L[i])
        RA = makeAscii(R[j])
        if(''.join(map(chr,compareAscii(LA, RA, 0))) == L[i]):
            A[k] = L[i]
            i=i+1
        else:
            A[k] = R[j]
            j = j+1
def makeAscii(A):
    arr = []
    for character in A:
        arr.append(ord(character))
    return arr

def compareAscii(A,B,n):
    if(n==0):
        A.append(-1000)
        B.append(-1000)
    if(A[n] < B[n]):
        A.pop()
        return A
    elif(A[n] > B[n]):
        B.pop()
        return B
    else:
        return compareAscii(A, B, n+1)

def QuickSort(A,p,r):
    if p<r:
        q=partiton(A,p,r)
        QuickSort(A,p,q-1)
        QuickSort(A,q+1,r)

def partiton(A,p,r):
    key = A[r]
    i=p-1
    for j in range(p,r):
        if A[j]<=key:
            i=i+1
            temp=A[i]
            A[i]=A[j]
            A[j]=temp
    change=A[i+1]
    A[i+1]=A[r]
    A[r]=change
    return i+1  

def InsertionSortDescending(array,start,end):
    for i in range(start, end):
        key=array[i]
        j=i-1
        while key> array[j] and j>=0:
            array[j+1]=array[j]
            j=j-1
        array[j+1]=key
    return array

def SelectionSortDescending (array,start,end):
    for i in range(start,end+1):
        maximum = i
        for j in range(i+1,end+1):
            if(array[maximum] < array[j]):
                temp = array[maximum]
                array[maximum] = array[j]
                array[j] = temp


def BubbleSortDescending(array,start,end):
    i=0
    swapped = True
    while i<end and swapped == True:
        swapped = False
        for j in range(start,end-i):
            if(array[j] < array[j+1]):
                temp = array[j]
                array[j] = array[j+1]
                array[j+1] = temp
                swapped = True
        i+=1

def QuickSortDescending(A,p,r):
    if p<r:
        q=partitonDescending(A,p,r)
        QuickSortDescending(A,p,q-1)
        QuickSortDescending(A,q+1,r)

def partitonDescending(A,p,r):
    key = A[r]
    i=p-1
    for j in range(p,r):
        if A[j]>=key:
            i=i+1
            temp=A[i]
            A[i]=A[j]
            A[j]=temp
    change=A[i+1]
    A[i+1]=A[r]
    A[r]=change
    return i+1 


# Using counting sort to sort the elements in the basis of significant places
def countingSort(array, place):
    size = len(array)
    output = [0] * size
    count = [0] * 10

    # Calculate count of elements
    for i in range(0, size):
        index = array[i] // place
        count[index % 10] += 1

    # Calculate cumulative count
    for i in range(1, 10):
        count[i] += count[i - 1]

    # Place the elements in sorted order
    i = size - 1
    while i >= 0:
        index = array[i] // place
        output[count[index % 10] - 1] = array[i]
        count[index % 10] -= 1
        i -= 1

    for i in range(0, size):
        array[i] = output[i]


# Main function to implement radix sort
def radixSort(array):
    # Get maximum element
    max_element = max(array)

    # Apply counting sort to sort elements based on place value.
    place = 1
    while math.floor(max_element / place) > 0:
        print("before",array,place)
        countingSort(array, place)
        place *= 10
        print(array,place)

def bucketSort(x):
	arr = []
	slot_num = 10 # 10 means 10 slots, each
				# slot's size is 0.1
	for i in range(slot_num):
		arr.append([])
	x,power=convertInFloat(x)	
    
	# Put array elements in different buckets
	for j in x:
		index_b = int(slot_num * j)
		arr[index_b].append(j)
	# Sort individual buckets
	for i in range(slot_num):
		arr[i] = InsertionSort(arr[i],0,len(arr[i]))
	# concatenate the result
	k = 0
	for i in range(slot_num):
		for j in range(len(arr[i])):
			x[k] = convert_Into_int(arr[i][j],power)
			k += 1
     
def convertInFloat(array):
    number=max(array)
    count=0
    while number!=0:
        number=number//10
        count+=1    
    for i in range(0,len(array)):
        array[i]=array[i]/math.pow(10,count)
    return array,count


def convert_Into_int(number,count):

        number=number*math.pow(10,count)
        return int(number)





