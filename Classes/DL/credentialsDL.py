AllUsers = []

class credentials:
    firstName = ""
    lastName = ""
    userName = ""
    password = ""
    email = ""
    role = ""
    
    def __init__(self,firstName,lastName,userName,password,email,role):
        self.firstName = firstName
        self.lastName = lastName
        self.userName = userName
        self.password = password
        self.email = email
        self.role = role


@staticmethod
def addCredentialsIntoList(myCredentials):
    AllUsers.append(myCredentials)
@staticmethod
def returnUser(myUser):
    for user in AllUsers:
        if(user.userName == myUser.userName and user.password == myUser.password):
            return user.role
    return None

