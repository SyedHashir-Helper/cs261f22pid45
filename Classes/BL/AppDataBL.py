# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 10:28:43 2022

@author: HS
"""

class App:
    Name = ""
    Category = ""
    Type = ""
    Version = ""
    Price = ""
    Description = ""
    
    def __init__(self,*args):
            self.Name = args[0]
            self.Category = args[1]
            self.Type = args[2]
            self.Version = args[3]
            self.Price = args[4]
            self.Description = args[5]
    